from typing import Optional

from pydantic import BaseModel, Field  # TODO Аналогично предыдущему

class BaseReciept(BaseModel):    # TODO Определение классо должно отделяться от остального кода двумя пустыми строками
    name: str
    time_of_prepare: int

class RecieptIn(BaseReciept):
    ...

class RecieptOut(BaseReciept):
    id: int
    count: Optional[int] = None

    class Config:
        orm_mode = True




