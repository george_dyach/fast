from typing import Dict, Optional, List

from fastapi import FastAPI, Path, Query, Body
from pydantic import Field, BaseModel
# import models
# TODO неиспользуемые импорты надо удалять

from . import models
from . import schemas

from .database import engine, session
from sqlalchemy.future import select
app = FastAPI()

@app.on_event("startup")  # TODO Определение функции должно отделяться от остального кода двумя пустыми строками
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.post('/reciept/', response_model=schemas.RecieptOut)
async def reciept_post(reciept: schemas.RecieptIn) -> models.Receipt:
    # TODO добавьте докстринги для каждого эндпоинта
    new_book = models.Receipt(**reciept.dict())
    new_book.count = 0
    async with session.begin():
        session.add(new_book)

    return new_book


@app.get('/reciept/', response_model=List[schemas.RecieptOut])
async def reciept() -> List[models.Receipt]:
    async with session.begin():
        res = await session.execute(select(models.Receipt).order_by(models.Receipt.count.desc()))
    return res.scalars().all()

@app.get('/reciept/{idx}', response_model=schemas.RecieptOut)  # TODO Аналогично предыдущему
async def reciept(idx: Optional[int] = None) -> models.Receipt:
    async with session.begin():
        res = await session.execute(select(models.Receipt).where(models.Receipt.id == idx))
        result=res.scalars().first()
        result.count+=1
        session.add(result)
    return result







